import {Pipe, PipeTransform} from "angular2/core";
@Pipe({
    name: 'timeFormat'
})

export class TimeFormatPipe implements PipeTransform {
    transform(value:any, args:any[]):any {
        let format = args[0];
        let output = '';
        let time = value;
        if (format.indexOf('Hours') != -1) {
            let hours = Math.floor(time / 3600000);
            output += hours + "h ";
            time = time - hours * 3600000;
        }
        if (format.indexOf('Minutes') != -1) {
            let minutes = Math.floor(time / 60000);
            output += minutes + "m ";
            time = time - minutes * 60000;
        }
        if (format.indexOf('Seconds') != -1) {
            let seconds = Math.floor(time / 1000);
            output += seconds + "s ";
            time = time - seconds * 1000;
        }
        if (format.indexOf('Milliseconds') != -1) {
            if (output.length) {
                output += time + "ms"
            }
            else {
                output = "" + time;
            }
        }

        if (!output.length) {
            output = "" + time;
        }
        return output;
    }

}