/**
 * Created by djradakovic on 3/19/2016.
 */
import {Component} from 'angular2/core';
import {SongComponent} from "../components/song/song.component";
import {RouteParams, Router} from "angular2/router";
import {SongsService} from "../service/songs.service";

@Component(
    {
        selector: 'item-view',
        templateUrl: 'templates/views/item.view.html',
        directives: [SongComponent],
        styles: ['li{padding-bottom: 10px; cursor: pointer;}']
    })

export class ItemView {
    constructor(private _routeParams:RouteParams,
                private _songsService:SongsService,
                private _router:Router) {
        this.song = {};
    }

    song;
    songsByArtist;

    ngOnInit() {
        //Must cast to int
        let id = +this._routeParams.get('id');
        this._songsService.song$.subscribe(song=> {
            this.song = song
        });
        this._songsService.songsByArtist$.subscribe(songsByArtist=>this.songsByArtist = songsByArtist);
        this._songsService.getSong(id);
    }

    changeSong(songId) {
        let link = ['Item', {id: songId}];
        this._router.navigate(link)
    }
}