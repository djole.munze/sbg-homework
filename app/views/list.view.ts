/**
 * Created by djradakovic on 3/19/2016.
 */
import {Component} from 'angular2/core';
import {SidebarComponent} from "../components/sidebar/sidebar.component";
import {SongsListComponent} from "../components/songs-list/songs-list.component";

@Component(
    {
        selector: 'list-view',
        templateUrl: 'templates/views/list.view.html',
        styles: ['.col {padding: 0}'],
        directives: [SidebarComponent, SongsListComponent]
    })

export class ListView {

}