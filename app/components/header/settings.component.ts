import {Component, Input} from 'angular2/core';
import {CheckListComponent} from "../shared/check-list.component";
import {TimeFormatService} from "../../service/time-format.service";

@Component(
    {
        selector: 'settings-component',
        templateUrl: 'templates/components/header/settings.template.html',
        directives: [CheckListComponent],
        styles: ['p{border-bottom: 1px dotted #999999;margin-top: 5px; padding-bottom: 15px}']
    })

export class SettingsComponent {
    @Input()
    opened;

    constructor(private _timeFormatService:TimeFormatService) {
    }

    checkListData = ['Hours', 'Minutes', 'Seconds', 'Milliseconds'];
    selectedItems = ['Minutes', 'Seconds'];

    isOpened = () => this.opened ? 'block' : 'none';

    filtersChange = function (filters) {
        this._timeFormatService.format = filters;
    }
}