/**
 * Created by djordjeradakovic on 3/18/16.
 */
import {Component} from 'angular2/core';
import {SettingsComponent} from "./settings.component";
import {ROUTER_DIRECTIVES} from "angular2/router";

@Component(
    {
        selector: 'header-component',
        templateUrl: 'templates/components/header/header.template.html',
        directives: [SettingsComponent, ROUTER_DIRECTIVES]
    })
export class HeaderComponent {
    opened = false;

    toggleSettings = function () {
        this.opened = !this.opened;
    }
}