/**
 * Created by djordjeradakovic on 3/22/16.
 */
import {Component, Input, Output, EventEmitter} from 'angular2/core';

@Component(
    {
        selector: 'rating',
        templateUrl: 'templates/components/shared/rating.template.html',
        styles: ['li{display: inline; margin-right: 3px; cursor: pointer} ul{margin: 0}']
    }
)

export class RatingComponent {
    @Output()
    changeRating = new EventEmitter();

    @Input()
    rate;
    @Input()
    size = 30;

    @Input()
    set base(val) {
        this.range = [];
        for (let i = 1; i <= val; i++) {
            this.range.push(i);
        }
    }

    //default
    range = [1, 2, 3];

    updateRating(val) {
        this.changeRating.emit(val);
    }

}