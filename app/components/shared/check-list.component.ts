/**
 * Created by djradakovic on 3/18/2016.
 */
import {Component, Input, Output, EventEmitter} from 'angular2/core';


@Component(
    {
        selector: 'check-list',
        templateUrl: 'templates/components/shared/filters-list.template.html'
    })

export class CheckListComponent {
    @Input()
    title;
    @Input()
    filters;
    @Input()
    horizontal;

    @Input()
    set selectedItems(items) {
        for (let item of items) {
            let pos = this.filters.indexOf(item);
            if (pos != -1) {
                this.selected[pos] = true;
            }
        }
        this.filtersChange();
    }

    @Output()
    changeFilters = new EventEmitter();

    selected = [];

    filtersChange() {
        //Bug in Angular 2
        //https://github.com/angular/angular/issues/3406
        window.setTimeout(()=> {
            var result = [];
            for (let i = 0; i < this.filters.length; i++) {
                if (this.selected[i]) {
                    result.push(this.filters[i]);
                }
            }
            this.changeFilters.emit(result)
        }, 100)
    }
}