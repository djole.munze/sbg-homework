import {Component, OnInit} from 'angular2/core';
import {SongsService} from "../../service/songs.service";
import {CheckListComponent} from "./../shared/check-list.component";

@Component(
    {
        selector: 'sidebar',
        templateUrl: 'templates/components/sidebar/sidebar.template.html',
        directives: [CheckListComponent],
        styles: ['label{width:100%}']
    })

export class SidebarComponent implements OnInit {
    ngOnInit():any {
        this._songsService.artists$.subscribe(artists=>this.artists = artists);
        this._songsService.genres$.subscribe(genres=>this.genres = genres);

    }

    constructor(private  _songsService:SongsService) {
    }

    artists = [];
    genres = [];

    selectedArtists = [];
    selectedGenres = [];

    filterArtists(artists) {
        this.selectedArtists = artists;
        this._songsService.filterSongs(this.selectedArtists, this.selectedGenres);
    }

    filterGenres(genres) {
        this.selectedGenres = genres;
        this._songsService.filterSongs(this.selectedArtists, this.selectedGenres);
    }

}