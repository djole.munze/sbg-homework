/**
 * Created by djradakovic on 3/18/2016.
 */
import {Component, OnInit} from 'angular2/core';
import {SongsService} from './../../service/songs.service'
import {Router} from "angular2/router";
import {RatingComponent} from "../shared/rating.component";
import {TimeFormatService} from "../../service/time-format.service";
import {TimeFormatPipe} from "../../pipes/time-format.pipe";


@Component(
    {
        selector: 'songs-list',
        templateUrl: 'templates/components/songs-list/songs-list.template.html',
        styles: [`.row .col {padding: 0}`],
        directives: [RatingComponent],
        pipes: [TimeFormatPipe]
    })

export class SongsListComponent implements OnInit {
    constructor(private  _songsService:SongsService,
                private _timeFormatService:TimeFormatService,
                private _router:Router) {
    }

    songs = [];
    format = [];

    ngOnInit() {
        this._songsService.getSongs().subscribe(songs=>this.songs = songs);
        this._songsService.loadSongs();
        this.format = this._timeFormatService.format;
        this._timeFormatService.formatChange.subscribe(format=>this.format = format);
    }

    openSong(songId) {
        let link = ['Item', {id: songId}];
        this._router.navigate(link)
    }

    updateSongRating(rating, songId) {
        console.log(rating, songId);
        this.songs.find(song=>song.id == songId).hearts = rating;
        this._songsService.updateSongRating(songId, rating);
    }
}