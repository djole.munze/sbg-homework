/**
 * Created by djradakovic on 3/19/2016.
 */
import {Component, Input, OnInit} from 'angular2/core';
import {ROUTER_DIRECTIVES} from "angular2/router";
import {RatingComponent} from "../shared/rating.component";
import {SongsService, Song} from "../../service/songs.service";
import {TimeFormatService} from "../../service/time-format.service";
import {TimeFormatPipe} from "../../pipes/time-format.pipe";


@Component(
    {
        selector: 'song',
        templateUrl: 'templates/components/song/song.template.html',
        styleUrls: ['css/song.css'],
        directives: [ROUTER_DIRECTIVES, RatingComponent],
        pipes: [TimeFormatPipe]
    })

export class SongComponent implements OnInit {
    @Input()
    song:Song;

    constructor(private _songsService:SongsService,
                private _timeFormatService:TimeFormatService) {
    }

    format = [];
    collapsed = true;

    ngOnInit() {
        this.format = this._timeFormatService.format;
        this._timeFormatService.formatChange.subscribe(format=>this.format = format);
    }

    toggleCollapsed() {
        this.collapsed = !this.collapsed;
    }

    updateSongRating(rating) {
        this._songsService.updateSongRating(this.song.id, rating);
    }
}