/**
 * Created by djordjeradakovic on 3/17/16.
 */
import {bootstrap}    from 'angular2/platform/browser';
import {AppComponent} from './app';
import 'rxjs/Rx';

bootstrap(AppComponent, []);