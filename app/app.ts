import {Component, provide} from 'angular2/core';
import {HTTP_PROVIDERS} from "angular2/http";
import {
    ROUTER_DIRECTIVES, ROUTER_PROVIDERS, RouteConfig, HashLocationStrategy,
    LocationStrategy
} from "angular2/router";

import {ItemView} from "./views/item.view";
import {ListView} from "./views/list.view";
import {SongsService} from './service/songs.service';
import {HeaderComponent} from "./components/header/header.component";
import {TimeFormatService} from "./service/time-format.service";


@Component({
    selector: 'music-box',
    templateUrl: 'templates/views/app.html',
    directives: [ListView, ItemView, HeaderComponent, ROUTER_DIRECTIVES],
    providers: [SongsService, TimeFormatService, HTTP_PROVIDERS, ROUTER_PROVIDERS,
        provide(LocationStrategy, {useClass: HashLocationStrategy})]
})
@RouteConfig([
    {path: '/list', name: 'List', component: ListView, useAsDefault: true},
    {path: '/song/:id', name: 'Item', component: ItemView}
])
export class AppComponent {

}
