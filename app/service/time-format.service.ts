import {Injectable, EventEmitter} from "angular2/core";

@Injectable()
export class TimeFormatService {

    private _format = [];
    formatChange = new EventEmitter();

    set format(format) {
        this._format = format;
        this.formatChange.emit(format);
    }

    get format() {
        return this._format;
    }
}