/**
 * Created by djordjeradakovic on 3/17/16.
 */
import {Injectable} from 'angular2/core';
import {Http} from "angular2/http";
import {Observable} from "rxjs/Observable";
import {Observer} from "rxjs/Observer";


//Added just for IDE so it don't complain, compiles and works fine even without it
export interface Song {
    artist:{name:String};
    genre:String;
    id;
    hearts;
}

@Injectable()
export class SongsService {
    songs$:Observable;
    private _songsObserver:Observer;

    artists$:Observable;
    private _artistsObserver:Observer;

    genres$:Observable;
    private _genresObserver:Observer;

    songs:Song[] = [];

    song$:Observable;
    private _songObserver:Observer;

    songsByArtist$:Observable;
    private _songsByArtistObserver:Observer;

    constructor(private _http:Http) {
        this.songs$ = new Observable(observer=>this._songsObserver = observer).share();
        this.artists$ = new Observable(observer=>this._artistsObserver = observer).share();
        this.genres$ = new Observable(observer=>this._genresObserver = observer).share();

        this.song$ = new Observable(observer=>this._songObserver = observer).share();
        this.songsByArtist$ = new Observable(observer=>this._songsByArtistObserver = observer).share();

    }

    private updateArtists(songs) {
        this._artistsObserver && this._artistsObserver.next(Array.from(new Set(songs.map(song => song.artist.name))));
    }

    private updateGenres(songs) {
        var genres = songs.map(song => song.genre.split("/"));
        this._genresObserver && this._genresObserver.next(Array.from(new Set(genres.reduce((a, b) => a.concat(b)))));
    }

    loadSongs() {
        if (!this.songs.length) {
            this._http.get('data/songs.json').map(response=>response.json()).subscribe(data => {
                this.songs = data;
                this._songsObserver.next(data);

                //create filters data
                this.updateArtists(data);
                this.updateGenres(data);
            })
        }
        else {
            this._songsObserver.next(this.songs);

            //Called before views are instantiated
            window.setTimeout(()=> {
                this.updateArtists(this.songs);
                this.updateGenres(this.songs);
            }, 0)

        }
    }

    getSongs() {
        return this.songs$;
    }


    getSong(id) {
        //don't load songs if we already have them
        if (this.songs.length) {
            let song = this.songs.find((song)=>song.id === id);
            this._songObserver.next(song);
            this.updateSongsByArtist(song.artist.name, song.id);
        }
        else {
            var subscription = this.songs$.subscribe(songs=> {
                this.songs = songs;
                let song = this.songs.find((song)=>song.id === id);
                this._songObserver.next(song);
                this.updateSongsByArtist(song.artist.name, song.id);
                subscription.unsubscribe();
            });
            this.loadSongs();
        }
    }

    filterSongs(artists, genres) {
        var filteredSongs = this.songs;
        if (artists.length) {
            filteredSongs = filteredSongs.filter(song => {
                return artists.indexOf(song.artist.name) != -1
            })
        }
        if (genres.length) {
            filteredSongs = filteredSongs.filter(song=> {
                for (let i = 0; i < genres.length; i++) {
                    if (song.genre.split('/').indexOf(genres[i]) != -1) {
                        return true;
                    }
                }
                return false;
            })
        }
        this._songsObserver.next(filteredSongs);
    }

    updateSongsByArtist(artist, songId) {
        this._songsByArtistObserver.next(this.songs.filter(song=>(song.artist.name == artist && song.id != songId)));
    }

    updateSongRating(songId, rating) {
        this.songs.find(song=>song.id == songId).hearts = rating;
    }


}

