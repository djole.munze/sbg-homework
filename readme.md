
**Specification**

You are tasked with building a single page app in a framework of your choice (or in vanilla JavaScript if you prefer), which should:

* Render a list of songs on startup and present a side panel which holds filters for filtering these songs by artist and genre
* The songs are available in the file songs.json and it’s perfectly fine to hard-code the data and mock all potential backend calls, including fetching songs, fetching the different genres, the different artists and rating a song (this all can happen on local data, as long as a real backend can be easily added later)
* Activating any of the filters on the right-hand side panel should filter the song list on the left-hand side
* It should be possible to change the rating for a song from the list view (again, the appropriate backend request should be mocked and changes should affect the already loaded data, to simplify it a bit)
* The filter lists should show scrollbars, if not all of the filter items fit into the available space
* The names of filter items in the list should automatically be truncated when they don’t fit in the available space (item labels should not line-break)
* Clicking on a song name in the list will open the details view for that song
* The details view shows a summary of the songs information, should allow the user to change the song rating and should contain a list of other songs by the artist on the right-hand side panel (use the songs from the same artists which are already in the original song list)
* The details view also contains additional album information, which should be presented in an accordion and initially be shown as collapsed (clicking on the label/header for the accordion should slide out the content of the accordion and the animation timing and timing-curve should be defined via CSS)

**Extra**: Our sample app has a little menu icon in the top right and on clicking the menu, it should present a dropdown in which the global time-format (used for displaying song durations) can be changed. Changing the format in the settings should automatically update the appropriate components which display song durations

Imagine this is an MVP and additional features are already planned for the future. Try structuring the application in a way, that you can easily extend it when the time comes and reuse as much of the code you have already written for this MVP. Try using a module system (requirejs, common-js or ES6 standard modules) to load your script files, rather than referencing each script with a script tag in your HTML.